import React, { Component } from "react";
import { Col } from "react-bootstrap";

class Stock extends Component {
  state = {};

  render() {
    const { stock } = this.props;
    const stockImg = `https://assets.smallcase.com/images/smallcases/80/${stock.scid}.png`;

    return (
      <Col
        className="stock"
        style={{
          display: "flex",
          flexDirection: "row",
          gap: "20px",
          borderBottom: "1px solid #bfbfbf",
          height: "150px",
          alignItems: "center",
        }}
      >
        <div style={{ flex: "10%" }}>
          <img src={stockImg}></img>
        </div>
        <div style={{ flex: "40%" }}>
          <h5>{stock.info.name}</h5>
          <div>{stock.info.shortDescription}</div>
          <div>by {stock.info.publisherName}</div>
        </div>
        <div style={{ flex: "10%" }}>
          <div>Min. Amount</div>
          <div>₹{stock.stats.minInvestAmount}</div>
        </div>
        <div style={{ flex: "10%" }}>
          <div>{stock.stats.ratios.cagrDuration} CAGR</div>
          <div style={{ color: stock.stats.ratios.cagr > 0 ? "green" : "red" }}>
            {(stock.stats.ratios.cagr * 100).toFixed(2)}%
          </div>
        </div>
        <div
          style={{
            border: "1px solid #bfbfbf",
            borderRadius: "5px",
            flex: "15%",
            padding: "5px",
          }}
        >
          <img
            src={`/images/${stock.stats.ratios.riskLabel.split(" ")[0]}.png`}
            style={{ width: "30px" }}
          />
          {stock.stats.ratios.riskLabel === "Medium Volatility"
            ? "Med. Volatility"
            : stock.stats.ratios.riskLabel}
        </div>
        <span class="material-icons save">bookmark</span>
      </Col>
    );
  }
}

export default Stock;
