import React, { Component } from "react";
import { Container } from "react-bootstrap";
import Stock from "./stock";
import Api from "../api";
import { Button } from "react-bootstrap";

class Stocks extends Component {
  state = {
    stocks: [],
  };

  componentDidMount() {
    const fetchData = async () => {
      try {
        const response = await Api.getAllStocks(this.props);
        this.setState({ stocks: response.data.data });
      } catch (err) {
        console.error(err);
      }
    };
    fetchData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.filters !== this.props.filters) {
      const fetchData = async () => {
        try {
          const response = await Api.getUpdateStocks(this.props.filters);
          this.setState({ stocks: response.data.data });
        } catch (err) {
          console.error(err);
        }
      };
      fetchData();
    }
  }

  render() {
    const { stocks } = this.state;
    console.log(this.props.filters.count);
    return (
      <Container>
        {stocks.length!==0 ? (
          stocks.map((stock) => <Stock key={stock._id} stock={stock} />)
        ) : (
          <div className="no-stocks">
          <h2>There are no stocks</h2>
          </div>
        )}
        {stocks.length % 10 === 0 && stocks.length !== 0 ? (
          <div className="loadmore">
            <Button
              variant="primary"
              size="lg"
              onClick={() => {
                this.props.onCount();
              }}
            >
              Load more..
            </Button>
          </div>
        ) : null}
      </Container>
    );
  }
}

export default Stocks;
