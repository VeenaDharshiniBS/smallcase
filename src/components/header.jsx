import React from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import "bootstrap/dist/css/bootstrap.min.css"
import Button from 'react-bootstrap/Button';
import logo from "./../logo-full.svg"
function Header() {
  return (
    <Navbar collapseOnSelect expand="lg" variant="light" style={{borderBottom :"1px solid #c5c5c5"}}>
    <Container >
      <Navbar.Brand  href="#home"><img src={logo}  width="150px" height="40px" alt='SmallCase'/></Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="me-auto" style={{gap:"10px"}}>
          <Nav.Link href="#Discover">Discover</Nav.Link>
          <Nav.Link href="#Watchlist">Watchlist</Nav.Link>
        </Nav>
        <Nav style={{gap:"10px"}}>
        <Nav.Link href="#bussiness">For bussiness</Nav.Link>
        <NavDropdown title="More" id="collasible-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">
              Another action
            </NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">
              Separated link
            </NavDropdown.Item>
          </NavDropdown>
          <Button size='sm' variant='outline-info' style={{padding: "2px 10px"}}>Login</Button>
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
  )
}

export default Header