import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import NavBar from "./navbar";
import SideContainer from "./sideContainer";
import Stocks from "./stocks";

class Discover extends Component {
  state = {
    filters: {
      sortBy: "",
      sortOrder: 1,
      risk: [],
      public: "",
      private: "",
      maxMinInvestAmount: "",
      sids: "",
      scids: "",
      excludeScids: "",
      count: 10,
      offset: 0,
      recentlyLaunched: undefined,
      searchString: "",
      investmentStrategies: [],
    },
    countFiltersApplied: 0,
    flagInvestAmount: 0,
    flagSubscription: 0,
    stratagyChecked: false
  };

  handleMaxMinInvestAmount = (values) => {
    this.setState({
      filters: { ...this.state.filters, maxMinInvestAmount: values },
    });
    if (values !== "Any") {
      if (this.state.flagInvestAmount === 0) {
        this.setState({
          countFiltersApplied: this.state.countFiltersApplied + 1,
          flagInvestAmount: 1,
        });
      }
    } else {
      this.setState({
        countFiltersApplied: this.state.countFiltersApplied - 1,
        flagInvestAmount: 0,
      });
    }
  };

  handleStragtegies = (e) => {
    if (e.target.checked) {
      this.setState({
        filters: {
          ...this.state.filters,
          investmentStrategies: [
            ...this.state.filters.investmentStrategies,
            e.target.value,
          ],
        },stratagyChecked:e.target.checked,
        countFiltersApplied: this.state.countFiltersApplied + 1,
      });
    } else {
      this.setState({
        filters: {
          ...this.state.filters,
          investmentStrategies: this.state.filters.investmentStrategies.filter(
            (strategy) => strategy !== e.target.value
          ),
        },stratagyChecked:e.target.checked,
        countFiltersApplied: this.state.countFiltersApplied - 1,
      });
    }
    console.log("is", this.state.filters.investmentStrategies);
  };

  handleLaunchDate = (e) => {
    if (e.target.checked) {
      this.setState({
        filters: { ...this.state.filters, recentlyLaunched: e.target.checked },
        countFiltersApplied: this.state.countFiltersApplied + 1,
      });
    } else {
      this.setState({
        filters: { ...this.state.filters, recentlyLaunched: undefined },
        countFiltersApplied: this.state.countFiltersApplied - 1,
      });
    }
  };

  handleSubscriptionType = (e) => {
    if (e.target.value == "Show all") {
      this.setState({
        filters: {
          ...this.state.filters,
          public: undefined,
          private: undefined,
        },
      });
    } else if (e.target.value == "Free access") {
      this.setState({
        filters: { ...this.state.filters, public: true, private: undefined },
      });
    } else {
      this.setState({
        filters: { ...this.state.filters, public: undefined, private: true },
      });
    }
    if (e.target.value !== "Show all") {
      if (this.state.flagSubscription === 0) {
        this.setState({
          countFiltersApplied: this.state.countFiltersApplied + 1,
          flagSubscription: 1,
        });
      }
    } else {
      this.setState({
        countFiltersApplied: this.state.countFiltersApplied - 1,
        flagSubscription: 0,
      });
    }
  };

  handleVolatility = (e) => {
    if (e.target.checked) {
      this.setState({
        filters: {
          ...this.state.filters,
          risk: [...this.state.filters.risk, e.target.value],
        },
        countFiltersApplied: this.state.countFiltersApplied + 1,
      });
    } else {
      this.setState({
        filters: {
          ...this.state.filters,
          risk: this.state.filters.risk.filter(
            (volatile) => volatile !== e.target.value
          ),
        },
        countFiltersApplied: this.state.countFiltersApplied - 1,
      });
    }
    console.log("volatile", this.state.filters.risk);
  };

  handleSortByCategory = (value) => {
    this.setState({ filters: { ...this.state.filters, sortBy: value } });
  };

  handleReturns = (value) => {
    this.setState({ filters: { ...this.state.filters, sortBy: value } });
  };

  handleSortOrder = (value) => {
    this.setState({ filters: { ...this.state.filters, sortOrder: value } });
  };
  handleCount = () => {
    this.setState({
      filters: { ...this.state.filters, count: this.state.filters.count + 10 },
    });
  };
  handleClear = () => {
    this.setState({filters:{...this.state.filters,investmentStrategies:[]}})
  }
  render() {
    return (
      <div style={{ marginTop: "40px" }}>
        <NavBar
          getSortByCategory={this.handleSortByCategory}
          getReturns={this.handleReturns}
          getSortOrder={this.handleSortOrder}
        />
        <div className="container">
          <div className="row">
            <div className="col-3">
              <SideContainer
                count={this.state.countFiltersApplied}
                getMaxMinInvestAmount={this.handleMaxMinInvestAmount}
                getStrategies={this.handleStragtegies}
                getLaunchDate={this.handleLaunchDate}
                getSubscriptionType={this.handleSubscriptionType}
                getVolatility={this.handleVolatility}
                onClear={this.handleClear}
                stratagyChecked={this.state.filters.investmentStrategies}
                investAmountChecked = {this.state.filters.maxMinInvestAmount}
              />
            </div>
            <div className="col-9">
              <Stocks filters={this.state.filters} onCount={this.handleCount} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Discover;
