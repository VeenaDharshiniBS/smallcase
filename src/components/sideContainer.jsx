import React, { Component } from "react";
import { ButtonGroup, Button, OverlayTrigger, Tooltip } from "react-bootstrap";

export default class SideContainer extends Component {
  render() {
    console.log(this.props.investAmountChecked);
    const subscription = [
      { name: "Show all" },
      { name: "Free access" },
      { name: "Fee based" },
    ];
    let investAmount = [
      { name: "Any", value: "Any" },
      { name: `Under 5,000`, value: "5000" },
      { name: "Under 25,000", value: "25000" },
      { name: "Under 50,000", value: "50000" },
    ];
    const volatility = [
      { name: "Low", value: "low" },
      { name: "Medium", value: "medium" },
      { name: "High", value: "high" },
    ];
    let investStratagy = [
      { name: "Asset Allocation", value: "assetAllocation" },
      { name: "Corporate Governance", value: "corporateGovernance" },
      { name: "Dividend", value: "dividend" },
      { name: "ESG", value: "esg" },
      { name: "Factor Investing", value: "factorInvesting" },
      { name: "Fundamental", value: "fundamental" },
      { name: "Goal based", value: "goalBased" },
      { name: "Growth", value: "growth" },
      { name: "Momentum", value: "momentum" },
      { name: "Quality", value: "quality" },
      { name: "Quantamental", value: "quantamental" },
      { name: "Quantitative", value: "quantitative" },
      { name: "Sector Tracker", value: "sectorTracker" },
      { name: "Technical", value: "technical" },
      { name: "Thematic", value: "thematic" },
      { name: "Value", value: "value" },
    ];

    const tooltipSubscription = (props) => (
      <Tooltip id="button-tooltip" {...props}>
        Based on access fees for constituents & rebalance updates
      </Tooltip>
    );

    const tooltipInvestAmount = (props) => (
      <Tooltip id="button-tooltip" {...props}>
        Select a range that you would like to invest
      </Tooltip>
    );

    const tooltipVolatility = (props) => (
      <Tooltip id="button-tooltip" {...props}>
        Based on volatility as compared to NIFTY50
      </Tooltip>
    );

    const tooltipLaunchDate = (props) => (
      <Tooltip id="button-tooltip" {...props}>
        Include smallcases that were launched less than a year ago
      </Tooltip>
    );

    const tooltipInvestmentStrategy = (props) => (
      <Tooltip id="button-tooltip" {...props}>
        Based on different kinds of market exposures
      </Tooltip>
    );

    return (
      <div className="side-container">
        <div className="filter">
          <div className="filter-header">
            Filter <div className="filter-count">{this.props.count}</div>
          </div>
          <div
            className="clear-all"
            onClick={() => {
              this.props.onClear();
            }}
          >
            {" "}
            Clear All
          </div>
        </div>
        <div className="titles">Subscription type</div>
        <ButtonGroup>
          {subscription.map((radio, idx) => (
            <Button
              key={idx}
              type="radio"
              variant="outline-primary"
              name="radio"
              value={radio.name}
              checked={idx==0 ? true : false}
              className="subscription-button"
              onClick={(e) => this.props.getSubscriptionType(e)}
            >
              {radio.name}
            </Button>
          ))}
        </ButtonGroup>
        <div className="titles">Invest Amount</div>
        <div className="titles">
          Subscription type
          <OverlayTrigger
            placement="right"
            delay={{ show: 250, hide: 400 }}
            overlay={tooltipSubscription}
          >
            <span style={{ cursor: "pointer" }} className="material-icons info">
              info
            </span>
          </OverlayTrigger>
          <ButtonGroup>
            {radios.map((radio, idx) => (
              <Button
                key={idx}
                type="radio"
                variant="outline-secondary"
                name="radio"
                value={radio.name}
                className="subscription-button"
                onClick={(e) => this.props.getSubscriptionType(e)}
              >
                {radio.name}
              </Button>
            ))}
          </ButtonGroup>
        </div>

        <div className="titles">
          Invest Amount
          <OverlayTrigger
            placement="right"
            delay={{ show: 250, hide: 400 }}
            overlay={tooltipInvestAmount}
          >
            <span style={{ cursor: "pointer" }} className="material-icons info">
              info
            </span>
          </OverlayTrigger>
        </div>
        <div
          className="invest-amount"
          onChange={(e) => this.props.getMaxMinInvestAmount(e.target.value)}
        >
          <label>
            <input
              type="radio"
              value="Any"
              name="invest-amount"
              defaultChecked
            />
            Any
          </label>
          <label>
            <input type="radio" value="5000" name="invest-amount" /> Under
            &#8377; 5,000
          </label>
          <label>
            <input type="radio" value="25000" name="invest-amount" /> Under
            &#8377; 25,000
          </label>
          <label>
            <input type="radio" value="50000" name="invest-amount" /> Under
            &#8377; 50,000
          </label>
        </div> */}
        <div>
          <div className="titles">Volatality</div>
          <Button variant="secondary" size="sm">
            <FontAwesomeIcon icon={faGauge} />
          </Button>
        </div>
        <div>
          <div className="titles">
            Launch Date
            <OverlayTrigger
              placement="right"
              delay={{ show: 250, hide: 400 }}
              overlay={tooltipLaunchDate}
            >
              <span
                style={{ cursor: "pointer" }}
                className="material-icons info"
              >
                info
              </span>
            </OverlayTrigger>
          </div>
          <form className="launch-date">
            <label>
              <input
                onChange={(e) => this.props.getLaunchDate(e)}
                type="checkbox"
                value="launchDate"
                name="launch-date"
              />
              Include new smallcases
            </label>
          </form>
        </div>
        <div>
          <div className="titles">
            Investment strategy
            <OverlayTrigger
              placement="right"
              delay={{ show: 250, hide: 400 }}
              overlay={tooltipInvestmentStrategy}
            >
              <span
                style={{ cursor: "pointer" }}
                className="material-icons info"
              >
                info
              </span>
            </OverlayTrigger>
          </div>
          <form className="stratagies">
            {investStratagy.map((stratagey) => {
              return (
                <div key={stratagey.name} className="stratagey">
                  <label>
                    <input
                      type="checkbox"
                      value={stratagey.value}
                      name={stratagey.name}
                      checked={stratagey?.isChecked || false}
                      className="stratagey-box"
                      onChange={(e) => this.props.getStrategies(e)}
                    />
                    {stratagey.name}
                  </label>
                </div>
              );
            })}
          </form>
        </div>
      </div>
    );
  }
}
