import React, { Component } from "react";
import { Navbar, Nav, Container, Form, Modal } from "react-bootstrap";

class NavBar extends Component {
  state = {
    showForm: false,
    showOrder: false,
  };

  handleOpenForm() {
    this.setState({ showForm: !this.state.showForm });
  }

  handleOpenOrder() {
    this.setState({ showOrder: true });
  }

  handleCloseOrder() {
    this.setState({ showOrder: false });
  }

  render() {
    const sortCategory = [
      { name: "Popularity", value: "popularity" },
      { name: "Minimum Amount", value: "minInvestAmount" },
      { name: "Recently Rebalanced", value: "rebalanceDate" },
    ];

    const returns = [
      { name: "1M", value: "monthlyReturns" },
      { name: "6M", value: "halfyearlyReturns" },
      { name: "1Y", value: "oneYearCagr" },
      { name: "3Y", value: "threeYearCagr" },
      { name: "5Y", value: "fiveYearCagr" },
    ];

    const sortOrders = [
      { name: "High-Low", value: -1 },
      { name: "Low-High", value: 1 },
    ];

    return (
      <Navbar>
        <Container style={{ borderBottom: "1px solid #bfbfbf" }}>
          <Nav style={{ gap: "30px" }}>
            <Nav.Link>Collections</Nav.Link>
            <Nav.Link style={{ borderBottom: "2px solid blue", color: "blue" }}>
              All smallcases
            </Nav.Link>
            <Nav.Link>Managers</Nav.Link>
          </Nav>

          <Nav style={{ gap: "40px" }}>
            <div>
              <div>
                <button className="btn" onClick={() => this.handleOpenForm()}>
                  SortBy
                </button>
              </div>

              {this.state.showForm && (
                <div
                  style={{
                    position: "absolute",
                    padding: "30px",
                    cursor: "pointer",
                    backgroundColor: "white",
                    border: "1px solid #c5c5c5",
                    borderRadius: "10px",
                    marginTop: "5px",
                  }}
                >
                  <div className="modal-body">
                    {sortCategory.map((category) => {
                      return (
                        <div
                          key={category.name}
                          name={category.name}
                          value={category.value}
                          onClick={() => {
                            this.props.getSortByCategory(category.value);
                            this.handleCloseOrder();
                          }}
                          className="category"
                        >
                          {category.name}
                        </div>
                      );
                    })}
                    <div>Returns</div>
                    <div>Time Period</div>
                    <div className="btn-group" role="group">
                      {returns.map((duration) => {
                        return (
                          <button
                            className="btn btn-outline-secondary"
                            value={duration.value}
                            key={duration.name}
                            onClick={() => {
                              this.props.getReturns(duration.value);
                              this.handleOpenOrder();
                            }}
                          >
                            {duration.name}
                          </button>
                        );
                      })}
                    </div>
                  </div>

                  <div>
                    {this.state.showOrder &&
                      sortOrders.map((order) => {
                        return (
                          <button
                            type="button"
                            className="btn btn-outline-secondary"
                            value={order.value}
                            key={order.name}
                            onClick={() => {
                              this.props.getSortOrder(order.value);
                            }}
                          >
                            {order.name}
                          </button>
                        );
                      })}
                  </div>
                </div>
              )}
            </div>

            <Form style={{ borderBottom: "2px solid black" }}>
              <Form.Group controlId="search">
                <Form.Control
                  type="text"
                  placeholder='Try "All Weather" or "Infosys"'
                  style={{ border: "none", outline: "none" }}
                />
              </Form.Group>
            </Form>
          </Nav>
        </Container>
      </Navbar>
    );
  }
}

export default NavBar;
