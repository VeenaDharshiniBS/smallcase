import axios from "axios";
const qs = require("qs");

class Api {
  getAllStocks(state) {
    return axios.get("smallcases/discover", {
      params: state,
    });
  }

  getUpdateStocks(state) {
    return axios.get("smallcases/discover", {
      params: state,
      paramsSerializer: {
        indexes: undefined,
      },
    });
  }
}

export default new Api();
