import "./App.css";

import Header from "./components/header";
import Discover from "./components/discover";

function App() {
  return (
    <>
      <Header />
      <Discover />
    </>
  );
}

export default App;
